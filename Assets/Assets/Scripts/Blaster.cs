﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Blaster : MonoBehaviour
{
    [SerializeField] private Bullet bullet;
    [SerializeField] private Transform muzzle;
    [SerializeField] private Transform blasterAnchor;
    [SerializeField] private float spreadForce;

    [SerializeField] public float ShootSpeed;
    [SerializeField] public float ReloadSpeed;
    [SerializeField] public float ReloadTimer = 0.0f;
    [SerializeField] public float ShootTimer = 0.0f;

    [SerializeField] private int maxAmmoCount = 100;
    [SerializeField] private float bulletImpulse = 1500;
    [SerializeField] private Text overBullet;
    [SerializeField] private Text tempBullet;
    [SerializeField] private Text reloadWarning;
    [SerializeField] private float rotationSpeed = 10;
    [SerializeField] float backforcePower = 10;
    [SerializeField] float maxShakeAmountCamera = 10;
    [SerializeField] float maxShakeAmountOther = 10;
    [SerializeField] private float decreaseFactor = 5;
    [SerializeField] private float decreaseFactorOther = 5;
    [SerializeField] private Transform[] objToShake;
    [SerializeField] private Vector3[] localPositionsOfShakeObjects;
    [SerializeField] private TextMesh ammoCounter;
    [SerializeField] private Player _player;
    [SerializeField] private AudioClip shotSound;
    [SerializeField] private AudioClip reloadSound;
    [SerializeField] private ParticleSystem reloadParticles;
    [SerializeField] private ParticleSystem shootParticles;
    [SerializeField] private Transform closeMuzzle;
    private Quaternion defaultRotation;

    private Rigidbody rigidbody;
    private float shakeAmount;
    private float shakeAmountOther;
    private Vector3 originalPos;
    private Quaternion startMuzzleRotation;
    private int curAmmoCount;
    private AudioSource audioSource;

    private bool reload = false;

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        startMuzzleRotation = muzzle.localRotation;
        //overBullet.text = curAmmoCount.ToString();
        //tempBullet.text = Ammo.ToString();
        //  reloadWarning.enabled = false;
        curAmmoCount = maxAmmoCount;
        originalPos = Camera.main.transform.localPosition;
        localPositionsOfShakeObjects = new Vector3[objToShake.Length];

        for (var i = 0; i < objToShake.Length; i++)
        {
            var transform1 = objToShake[i];
            localPositionsOfShakeObjects[i] = transform1.localPosition;
        }

        rigidbody = GetComponent<Rigidbody>();
        _player = GetComponent<Player>();
        defaultRotation = blasterAnchor.localRotation;
    }

    private void UpdateUI()
    {
        ammoCounter.text = curAmmoCount.ToString();
    }


    // Update is called once per frame
    void LateUpdate()
    {
        TryToShoot();
        UpdateUI();

        if (_player.Grounded)
        {
            Reload();
        }

        if (shakeAmount > 0 || shakeAmountOther > 0)
        {
            Camera.main.transform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
            for (var i = 0; i < objToShake.Length; i++)
            {
                var transform1 = objToShake[i];
                transform1.localPosition = localPositionsOfShakeObjects[i] + Random.insideUnitSphere * shakeAmountOther;
            }

            shakeAmount -= decreaseFactor;
            shakeAmountOther -= decreaseFactorOther;
        }
        else
        {
            shakeAmount = 0f;
            shakeAmountOther = 0f;
            Camera.main.transform.localPosition = originalPos;

            for (var i = 0; i < objToShake.Length; i++)
            {
                var transform1 = objToShake[i];
                transform1.localPosition = localPositionsOfShakeObjects[i];
            }
        }
    }

    private void Shoot(Quaternion rotation, BaseFlyingMonster enemy)
    {
        Bullet bulletInstance;

        if (enemy != null)
        {
            enemy.DealDamage(bullet.damage);
        }
        //    Vector3 rayOrigin = new Vector3(0.5f, 0.5f, 0f); // center of the screen
        //    float rayLength = 500f;

        //    // actual Ray
        //    Ray ray = Camera.main.ViewportPointToRay(rayOrigin);

        //    bulletInstance = Instantiate(bullet, closeMuzzle.position, rotation);

        //}
        //else
        //{

        bulletInstance = Instantiate(bullet, muzzle.position, rotation);

        //  }

        bulletInstance.GetComponent<Rigidbody>().AddForce(bulletImpulse * bulletInstance.transform.forward);
        rigidbody.AddForce(-muzzle.forward * backforcePower);
        shakeAmount = maxShakeAmountCamera;
        shakeAmountOther = maxShakeAmountOther;
        curAmmoCount = curAmmoCount - 1;
        ShootTimer = ShootSpeed;
        audioSource.clip = shotSound;
       // shootParticles.Play();
//        if(!audioSource.isPlaying)
        audioSource.volume = 0.1f;
        audioSource.Play();
    }

    private void TryToShoot()
    {
        Vector3 rayOrigin = new Vector3(0.5f, 0.5f, 0f); // center of the screen
        float rayLength = 500f;

        // actual Ray
        Ray ray = Camera.main.ViewportPointToRay(rayOrigin);
        RaycastHit hit;


        float xSpread = Random.Range(-1, 1);
        float ySpread = Random.Range(-1, 1);
        //normalize the spread vector to keep it conical
        Vector3 spread = new Vector3(xSpread, ySpread, 0.0f).normalized * spreadForce;
        Quaternion rotation = Quaternion.Euler(spread) * muzzle.rotation;
        BaseFlyingMonster enemy = null;

        if (Physics.Raycast(ray, out hit, rayLength))
        {
            
            if ((hit.point - transform.position).sqrMagnitude > 30)
            {
                muzzle.LookAt(hit.point);
                enemy = hit.transform.GetComponent<BaseFlyingMonster>();
                rotation = Quaternion.Euler(spread) * muzzle.rotation;
            }
            else
            {
                var endpoint = ray.origin + (ray.direction * 500);
                muzzle.localRotation = startMuzzleRotation;

                Utility.SmoothLoockAt(endpoint, blasterAnchor, rotationSpeed / 5);
            }

            if (hit.transform.tag != "Player" && (hit.point - transform.position).sqrMagnitude > 50)
            {
                Utility.SmoothLoockAt(hit.point, blasterAnchor, rotationSpeed);
            }
            else
            {
                var endpoint = ray.origin + (ray.direction * 50);

                Utility.SmoothLoockAt(endpoint, blasterAnchor, rotationSpeed / 5);
            }
        }
        else
        {
            var endpoint = ray.origin + (ray.direction * 500);
            muzzle.localRotation = startMuzzleRotation;

            Utility.SmoothLoockAt(endpoint, blasterAnchor, rotationSpeed / 5);
        }

        if (curAmmoCount > 0 && ReloadTimer <= 0 && ShootTimer <= 0)
        {
            if (Input.GetMouseButton(0))
            {
                Shoot(rotation, enemy);
            }
        }

        if (ShootTimer > 0)
        {
            ShootTimer -= Time.deltaTime;
        }

        if (ReloadTimer > 0)
        {
            ReloadTimer -= Time.deltaTime;
        }
        else
        {
            if (reload)
            {
                curAmmoCount = maxAmmoCount;
                reload = false;
            }
        }
    }

    private void Reload()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (ShootTimer > 0)
            {
                ShootTimer -= Time.deltaTime;
            }

            if (ReloadTimer > 0)
            {
                ReloadTimer -= Time.deltaTime;
            }
            else
            {
                ReloadTimer = ReloadSpeed;
                reload = true;
                audioSource.clip = reloadSound;
                audioSource.volume = 0.5f;

                if (!audioSource.isPlaying)
                    audioSource.Play();
                reloadParticles.Play();
            }
        }
    }
}