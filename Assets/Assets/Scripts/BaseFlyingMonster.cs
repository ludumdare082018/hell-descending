﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseFlyingMonster : MonoBehaviour
{
    [SerializeField] private float speed = 5;
    [SerializeField] private float health = 15;
    [SerializeField] private float distanceToReact = 100;
    [SerializeField] private float distanceToStop = 20;
    [SerializeField] private float distanceToShoot = 40;
    [SerializeField] private Missle missle;
    [SerializeField] private float shootingRate = 5;
    [SerializeField] private ParticleSystem death;
    [SerializeField] private List<AudioClip> moans;
    [SerializeField] private List<AudioClip> shoots;
    [SerializeField] private float moanFrequent = 3;
    [SerializeField] private AudioSource shotSource;
    public Vector3 GivenVelocity;
    private bool isActive = false;
    private Transform player;
    private Rigidbody rigidbody;
    private float lastShotTime = 0;
    private Animator animator;
    private float curSpeed;
    private AudioSource audioSource;
    private float lastMoan;
    private float curMoanFrequent;

    public bool IsActive
    {
        get { return isActive; }
    }

    public Vector3 Velocity
    {
        get { return rigidbody.velocity; }
    }

    // Use this for initialization
    void Awake()
    {
        player = GameObject.Find("player").transform;
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        moanFrequent = moanFrequent + Random.Range(0, moanFrequent * 2);
        speed -= Random.Range(0, .2f) * speed;
    }

    // Update is called once per frame
    void Update()
    {

        transform.LookAt(player.transform);


        var velocity = player.position - transform.position;

        var ray = new Ray(transform.position,velocity);
        Debug.DrawRay(transform.position,velocity);

        var distanceToPlayer = velocity.magnitude;

        if (distanceToPlayer < 50 && Time.timeSinceLevelLoad - lastShotTime > curMoanFrequent && !audioSource.isPlaying)
        {
            var c = moans[Random.Range(0, moans.Count)];
            curMoanFrequent = moanFrequent + Random.Range(1, moanFrequent * 2);

            audioSource.clip = c;
            audioSource.Play();
        }

        RaycastHit hit;
        if ((Time.timeSinceLevelLoad - lastShotTime) > shootingRate && Physics.Raycast(ray, out hit, distanceToShoot))
        {
            if(hit.transform.tag == "Player")
                TryToShoot();
        }

        if (distanceToPlayer > distanceToReact || distanceToPlayer < distanceToStop)
        {
            rigidbody.velocity = Vector3.zero;
            
            isActive = false;
            return;
        }

        isActive = true;
        //curSpeed = Mathf.Lerp(((distanceToPlayer - distanceToStop) / distanceToStop) * speed,curSpeed,0.999f);
        curSpeed = ((distanceToPlayer - distanceToStop) / (distanceToReact - distanceToStop)) * speed;

        velocity = velocity.normalized * curSpeed;

        rigidbody.velocity = velocity + GivenVelocity;
    }

    private void TryToShoot()
    {
        animator.SetTrigger("Fire");
    }

    private void Shoot()
    {
        var m = Instantiate(missle, transform.position + transform.forward, Quaternion.identity);
        lastShotTime = Time.timeSinceLevelLoad;

        shotSource.clip = shoots[0];
        shotSource.Play();

        m.Shoot(player.position);
    }

    public void DealDamage(float damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Instantiate(death, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}