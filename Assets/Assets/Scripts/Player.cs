﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Player : RigidbodyFirstPersonController
{
    [SerializeField] public float hp;
    [SerializeField] private AudioClip hitSound;
    [SerializeField] private float healingRate = 10;
    public Action OnDeath;
    [SerializeField]  AudioSource audioSource;
    private float maxHp;
    protected override void Start()
    {
        base.Start();
        maxHp = hp;
        //audioSource = GetComponent<AudioSource>();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (Grounded)
        {
            mouseLook.clampVerticalRotation = true;
            hp = Mathf.Clamp(hp + Time.deltaTime * healingRate, -100, maxHp);
        }
        else
        {
            mouseLook.clampVerticalRotation = false;
        }
    }

    public void DealDamage(float damage)
    {
        hp -= damage;
        audioSource.volume = 1;
        audioSource.clip = hitSound;
        audioSource.Play();
        if (hp <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        if(OnDeath != null)
            OnDeath.Invoke();
    }
}