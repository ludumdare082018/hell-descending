﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missle : MonoBehaviour
{
    [SerializeField] private float shootForse;
    [SerializeField] private float damage;
    private Transform target;
    private Rigidbody rigidbody;

    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    public void Shoot(Vector3 shootAt)
    {
        rigidbody.AddForce((shootAt - transform.position).normalized * shootForse);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.name == "player")
        {
            collision.transform.GetComponent<Player>().DealDamage(damage);
        }
    }
}
