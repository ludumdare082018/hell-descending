﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    [SerializeField] private List<AudioClip> music;
	// Use this for initialization

    private AudioSource ass;

	void Start ()
	{
	    ass = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (!ass.isPlaying)
	    {
	        ass.clip = music[Random.Range(0, music.Count)];
            ass.Play();
	    }
	}
}
