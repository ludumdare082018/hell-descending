﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility{

    public  static void SmoothLoockAt(Vector3 tartgetPosition, Transform transform, float damping)
    {
        //Vector3 lTargetDir = tartgetPosition - transform.position;
        //lTargetDir.y = 0.0f;
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lTargetDir), Time.time * m_Speed);

        Quaternion rotation = Quaternion.LookRotation(tartgetPosition - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
    }

    public static void SmoothRotate(Quaternion rotation, Transform transform, float damping)
    {
        transform.localRotation = Quaternion.Slerp(transform.localRotation, rotation, Time.deltaTime * damping);
    }
}
