﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class UIManager : MonoBehaviour
{    
    [SerializeField] private Player player;
    [SerializeField] private Transform deathScreen;
    [SerializeField] private Transform winScreen;
    [SerializeField] private Image blood;
    private bool dead = false;
    private float maxHp;
    void Awake()
    {
        player.OnDeath += OnDeath;
        maxHp = player.hp;
    }

    private void OnDeath()
    {
        deathScreen.gameObject.SetActive(true);
        blood.color = new Color(blood.color.r, blood.color.g, blood.color.b, 0);

        dead = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Cursor.visible = !Cursor.visible;

            player.mouseLook.SetCursorLock(!player.mouseLook.lockCursor);
        }

        if (Cursor.visible)
        {
            Cursor.visible = !Cursor.visible;

            player.mouseLook.SetCursorLock(!player.mouseLook.lockCursor);
        }

            if (player.transform.position.y < 80 && ! dead)
            OnDeath();

        if (dead && Input.GetKeyDown(KeyCode.Space))
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        blood.color =new Color(blood.color.r,blood.color.g,blood.color.b,0.7f- player.hp / maxHp);
    }

    public void GameWon()
    {
        winScreen.gameObject.SetActive(true);
        blood.color = new Color(blood.color.r, blood.color.g, blood.color.b, 0);
        dead = true;

    }
}