﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] private float enemySpawnrate = 1;
    [SerializeField] private float distanceToEnableSteer = 30;
    [SerializeField] private BaseFlyingMonster[] monsters;

    [SerializeField] private float seperationForce = 1;
    
    [SerializeField] private float aligmentForce = 1;

    void Awake()
    {
        monsters = GetComponentsInChildren<BaseFlyingMonster>();
    }

	void Update () {

        var activeMonsters = new List<BaseFlyingMonster>();
        var monstersToRemove = new List<BaseFlyingMonster>();

	    monsters = monsters.Where(r => r != null).ToArray();

        foreach (var baseFlyingMonster in monsters)
	    {
	        if (baseFlyingMonster.IsActive)
	        {
	            activeMonsters.Add(baseFlyingMonster);
	        }
	    }

        foreach (var activeMonster in activeMonsters)
	    {
            Vector3 givenVelocity = Vector3.zero;

	        int neighborCount = 0;

	        foreach (var otherActiveMonster in activeMonsters)
	        {
	            if ((activeMonster.transform.position - otherActiveMonster.transform.position).sqrMagnitude <
	                distanceToEnableSteer * distanceToEnableSteer && activeMonster != otherActiveMonster)
	            {
	                 givenVelocity += otherActiveMonster.transform.forward * aligmentForce;	                 
	                 givenVelocity -= 1/(otherActiveMonster.transform.position - activeMonster.transform.position).sqrMagnitude
	                                  * (otherActiveMonster.transform.position - activeMonster.transform.position).normalized * seperationForce;

                     neighborCount++;
                }
	        }

            if(neighborCount == 0)
                continue;
	        
	        givenVelocity /= neighborCount;
	        activeMonster.GivenVelocity = givenVelocity;
        }
	}
}
