﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] public float damage = 5;
    [SerializeField] public float impulseDivider = 5;
    [SerializeField] private ParticleSystem blood;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Enemy")
        {
            var enemy =  collision.transform.GetComponent<BaseFlyingMonster>();

            enemy.DealDamage(damage);
            

            Instantiate(blood.transform, collision.contacts[0].point, Quaternion.identity);

            Destroy(gameObject);
        }
    }
}
