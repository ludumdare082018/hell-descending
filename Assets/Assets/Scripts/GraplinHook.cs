﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public enum HookStrenghtType
{
    CONSTANT,
    DISTANCE,
    DISTANCE_SQR,
    DISTANCE_INVERSE,
    DISTANCE_INVERSE_SQR
}

public class GraplinHook : MonoBehaviour
{
    [SerializeField] private HookStrenghtType hookStrenghtType;
    [SerializeField] private float graplinPower = 5;
    [SerializeField] private float reverceVeloity = 5;
    [SerializeField] private float garivtyForce = 4;
    [SerializeField] private float vercicalForce = 4;
    [SerializeField] private float baseVercicalForce = 40;
    [SerializeField] private Text debugForceText;
    [SerializeField] private float timeToApplyVelocitybonus = 1;
    [SerializeField] private Transform muzzle;
    [SerializeField] private Transform hookAnchor;
    [SerializeField] private float allowedUseTime = 5;
    [SerializeField] private float requiredChardgeToFire = 0.3f;
    [SerializeField] private Cat cat;
    [SerializeField] private float rotationSpeed = 10;
    [SerializeField] private float lineWidth = 0.10f;
    [SerializeField] private Transform powerMeter;
    [SerializeField] private AudioClip shoot;
    [SerializeField] private float minShotDelay = 0.2f;
    public Color c1 = Color.yellow;
    public Color c2 = Color.red;

    private float attachedTime = 0;
    private float releaseTime = 0;
    private float holdingTime = 0;
    private float lunchTime = 0;
    private Camera camera;
    private LineRenderer lineRenderer;
    private Vector3 target = Vector3.zero;
    private Rigidbody rigidbody;
    private Quaternion defaultRotation;
    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        camera = GetComponent<Camera>();
        rigidbody = GetComponent<Rigidbody>();
        lineRenderer = GetComponent<LineRenderer>();
        defaultRotation = hookAnchor.localRotation;
       //c Physics.gravity = Vector3.down * 6.8f;
    }

    // Use this for initialization
    void Start()
    {
        lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.widthMultiplier = 0.2f;

        float alpha = 1.0f;
        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[] {new GradientColorKey(c1, 0.0f), new GradientColorKey(c2, 1.0f)},
            new GradientAlphaKey[] {new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f)}
        );
        lineRenderer.colorGradient = gradient;
    }

    void UpdatePowerMeter()
    {
        powerMeter.localScale = new Vector3(1, 1, 1 - holdingTime / allowedUseTime);
    }

    void RotateHook()
    {
        Vector3 rayOrigin = new Vector3(0.5f, 0.5f, 0f); // center of the screen
        float rayLength = 5000f;


        Ray ray = Camera.main.ViewportPointToRay(rayOrigin);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, rayLength))
        {
            if (hit.transform.tag != "Player" && (hit.point - transform.position).sqrMagnitude > 50)
            {
                Utility.SmoothLoockAt(hit.point, hookAnchor, rotationSpeed);
            }
            else
            {
                Utility.SmoothRotate(defaultRotation, hookAnchor, rotationSpeed / 7);
            }
        }
        else
        {
            Utility.SmoothRotate(defaultRotation, hookAnchor, rotationSpeed / 7);
        }
    }

    void UpdateLineSize()
    {
        if (holdingTime == 0)
            lineRenderer.widthMultiplier = lineWidth;
        else
            lineRenderer.widthMultiplier = lineWidth * (1 - holdingTime / allowedUseTime);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        target = Vector3.zero;
        Vector3 force = Vector3.zero;

        UpdateLineSize();
        UpdatePowerMeter();

        if (Input.GetMouseButton(1) && (holdingTime <= allowedUseTime ||
                                        (cat.AtBasePosition && holdingTime > allowedUseTime * requiredChardgeToFire)))
        {
            if (cat.AtBasePosition && holdingTime == 0 && (Time.timeSinceLevelLoad - lunchTime) > minShotDelay)
            {
                cat.Shoot(Vector3.zero);
                audioSource.clip = shoot;
                audioSource.volume = 1;
                lunchTime = Time.timeSinceLevelLoad;
                audioSource.Play();
            }
            else
            {
            }

            if (cat.AtBasePosition && holdingTime != 0)
            {
                if (releaseTime == 0)
                    releaseTime = Time.timeSinceLevelLoad;

                target = Vector3.zero;
                attachedTime = 0;
                holdingTime = Mathf.Clamp(holdingTime - (Time.timeSinceLevelLoad - releaseTime), 0, allowedUseTime);
            }
            else
            {
                releaseTime = 0;
                if (cat.Attached)
                {
                    force += Vector3.up * vercicalForce;

                    target = cat.transform.position;

                    if (attachedTime == 0)
                    {
                        attachedTime = Time.timeSinceLevelLoad;
                    }
                    else
                    {
                        holdingTime = Time.timeSinceLevelLoad - attachedTime;
                    }
                }
            }
        }
        else
        {
            if (releaseTime == 0)
                releaseTime = Time.timeSinceLevelLoad;

            target = Vector3.zero;
            attachedTime = 0;
            holdingTime = Mathf.Clamp(holdingTime - (Time.timeSinceLevelLoad - releaseTime), 0, allowedUseTime);

            cat.Return();
        }


        //if (Input.GetKey(KeyCode.Space))
        //{
        //    Debug.LogWarning(Time.timeSinceLevelLoad);
        //    force += Vector3.up * vercicalForce;
        //}

        if (cat.Attached || (Time.timeSinceLevelLoad - lunchTime) > minShotDelay)
        {
            lineRenderer.SetPositions(new Vector3[] { Vector3.back * 500000, Vector3.back * 500000 });

        }

        force += Vector3.up * baseVercicalForce;

        rigidbody.AddForce(force, ForceMode.Force);

        ApplyHook(target);
    }

    void ApplyHook(Vector3 target)
    {
        RotateHook();

        if (target != Vector3.zero)
        {
            Vector3 force;
            hookAnchor.LookAt(target);

            switch (hookStrenghtType)
            {
                case HookStrenghtType.CONSTANT:
                    force = (target - transform.position).normalized * graplinPower;
                    break;
                case HookStrenghtType.DISTANCE:
                    force = (target - transform.position) * graplinPower;
                    break;
                case HookStrenghtType.DISTANCE_INVERSE:
                    force = (1 / (target - transform.position).magnitude) * graplinPower *
                            (target - transform.position);

                    break;
                case HookStrenghtType.DISTANCE_INVERSE_SQR:
                    force = (1 / (target - transform.position).sqrMagnitude) * graplinPower *
                            (target - transform.position);

                    break;
                case HookStrenghtType.DISTANCE_SQR:
                    force = Mathf.Sqrt((target - transform.position).magnitude) *
                            (target - transform.position).normalized * graplinPower;

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            force -= rigidbody.velocity * rigidbody.mass / reverceVeloity;

            if (holdingTime < timeToApplyVelocitybonus)
            {
                force += force.normalized * rigidbody.velocity.magnitude;
            }

            rigidbody.AddForce(force, ForceMode.Force);
        }

        lineRenderer.SetPositions(new Vector3[] {muzzle.position, cat.transform.position});
    }

    Vector3 GetGraplinTarget()
    {
        Vector3 rayOrigin = new Vector3(0.5f, 0.5f, 0f); // center of the screen
        float rayLength = 500f;

        // actual Ray
        Ray ray = Camera.main.ViewportPointToRay(rayOrigin);

        // debug Ray
        Debug.DrawRay(ray.origin, ray.direction * rayLength, Color.red);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, rayLength))
        {
            return hit.point;
        }

        return Vector3.zero;
    }
}