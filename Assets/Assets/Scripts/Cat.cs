﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour
{
    [SerializeField] private float shootForce = 15;
    [SerializeField] private float distanceToMiss = 50;
    [SerializeField] private Transform muzz;

    private Rigidbody rigidbody;
    private bool atBasePosition = true;
    private Vector3 startPos;
    private Quaternion startRotation;
    private Transform target;
    private Vector3 contactPoint;
    private bool attached = false;
    private float distanceToMissSqr;
    private Vector3 relativePoint;

    public bool Attached
    {
        get { return attached; }
    }

    public bool AtBasePosition
    {
        get { return atBasePosition; }
    }

    void Awake()
    {
        distanceToMissSqr = distanceToMiss * distanceToMiss;
        startPos = transform.localPosition;
        startRotation = transform.localRotation;
        rigidbody = GetComponent<Rigidbody>();
        
    }

    public bool Shoot(Vector3 inheritVelocity)
    {
        if (!AtBasePosition)
            return false;

        rigidbody.isKinematic = false;

        rigidbody.AddForce(transform.forward * shootForce + inheritVelocity);
        rigidbody.detectCollisions = true;
        atBasePosition = false;

        return true;
    }

    public void Return()
    {
        transform.position = muzz.position;
        transform.rotation = muzz.rotation;
        rigidbody.detectCollisions = false;
        attached = false;
        atBasePosition = true;
        rigidbody.isKinematic = true;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!AtBasePosition && collision.transform.tag != "Player")
        {
            attached = true;
            target = collision.transform;
            contactPoint = collision.contacts[0].point;
            relativePoint = target.InverseTransformPoint(contactPoint);
            rigidbody.isKinematic = true;
            rigidbody.velocity = Vector3.zero;
            rigidbody.detectCollisions = false;            
        }
    }

    void LateUpdate()
    {
        if (!AtBasePosition)
        {
            if ((transform.position - muzz.position).sqrMagnitude > distanceToMissSqr)
            {
                Return();
            }

            if (Attached)
            {
                if (target == null)
                {
                    Return();
                    return;
                    
                }

                contactPoint = target.TransformPoint(relativePoint);
                transform.position = contactPoint;
            }
        }
        else
        {
            transform.position = muzz.position;
            transform.rotation = muzz.rotation;
        }
    }
}